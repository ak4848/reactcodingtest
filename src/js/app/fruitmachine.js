import React, { Component } from 'react';

export default class FruitMachine extends Component {
  constructor() {
    super();
    this.state = {
      wheelColours: new Array(3).fill('')
    };
  }

  componentWillMount() {
    this.spin();
  }

  spin = () => {
    const colours = ['yellow', 'red', 'blue', 'green'];
    this.setState({ wheelColours: this.state.wheelColours.map(() =>
      colours[Math.floor(Math.random() * colours.length)]
    )
    });
  }

  render() {
    const sameCols = this.state.wheelColours.filter((colour, i, arr) => colour === arr[0]).length;
    return (
      <div className='fruit-machine'>
        <p className='win'>{ sameCols === 3 ? 'WINNER' : '' } </p>
        <div className='wheels'>
          {this.state.wheelColours.map((colour, i) => {
            return (
              <div className={ `slot ${ colour }` } key={ `${ colour }${ i }` } />
            );
          })
        }
        </div>
        <button className='spin-btn' onClick={ this.spin }>PLAY</button>
      </div>
    );
  }
}
