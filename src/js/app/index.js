import React, { Component } from 'react';
import FruitMachine from './fruitmachine';

class App extends Component {
  render() {
    return (
      <div className='app-container'>
        <FruitMachine />
      </div>
    );
  }
}

export default App;
